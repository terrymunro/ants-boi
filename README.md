# BOI

The bots name is BOI as a reference to the PlayStation game God of War (2018) and the project avatar
is from my favorite twitch streamer Admiral Bahroo https://www.twitch.tv/collections/r2L-vluFMRUwlQ

Code based on: https://github.com/mk0x9/ants-haskell

Haskell starter bot for the 2011 Google AI Challenge - Ants

Reimplement the doTurn function in MyBot.hs, just don't change its signature.
Look in Ants.hs to see what data is available from GameParams and GameState.

Invoke ghc with optimizations turned on with the -O flag. This eliminates
unnecessary copying of arrays, see unsafeFreeze and unsafeThaw at
http://hackage.haskell.org/packages/archive/array/latest/doc/html/Data-Array-MArray.html#7
