module Main where

import Bot.BOI (doTurn)
import Game.Ants (game)

-- | This runs the game
main :: IO ()
main = game doTurn
