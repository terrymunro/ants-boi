module Spec
where

import Test.Tasty (defaultMain)

import BOISpec (tests)

-- Main program
main :: IO ()
main = defaultMain tests 
