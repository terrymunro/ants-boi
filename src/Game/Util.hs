module Game.Util
  ( fAnd
  , fOr
  , tuplify2
  ) where

--------------------------------------------------------------------------------
-- Utils that came with the starter package

fAnd :: a -> [a -> Bool] -> Bool
fAnd x = all ($x)

fOr :: a -> [a -> Bool] -> Bool
fOr x = any ($x)

tuplify2 :: Monoid a => [a] -> (a,a)
tuplify2 [x,y] = (x,y)
tuplify2 _ = (mempty, mempty)
