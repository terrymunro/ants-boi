module Game.Game
  ( GameState (..)
  , GameParams (..)
  , Order (..)

  , timeRemaining
  , passable
  ) where

import Control.Applicative ()
import Control.Monad.ST

import Data.Array.Unsafe (unsafeThaw)
import Data.List         (isPrefixOf, foldl')
import Data.Char         (digitToInt)
import Data.Time.Clock   (UTCTime, NominalDiffTime, getCurrentTime, diffUTCTime)

import System.IO

import Game.Point (Point)
import Game.World (Ant, Food, Hill)

--------------------------------------------------------------------------------
-- Game Parameters -------------------------------------------------------------
--------------------------------------------------------------------------------
data GameParams = GameParams
  { loadtime      :: Int
  , turntime      :: Int
  , rows          :: Int
  , cols          :: Int
  , turns         :: Int
  , playerSeed    :: Int
  , viewradius2   :: Int
  , attackradius2 :: Int
  , spawnradius2  :: Int
  , viewCircle    :: [Point]
  , attackCircle  :: [Point]
  , spawnCircle   :: [Point]
  } deriving (Show)

createParams :: [(String, String)] -> GameParams
createParams s =
  let lookup' key = read $ fromJust $ lookup key s
      vr2 = lookup' "viewradius2"
      ar2 = lookup' "attackradius2"
      sr2 = lookup' "spawnradius2"
      vp = getPointCircle vr2
      ap = getPointCircle ar2
      sp = getPointCircle sr2
  in GameParams { loadtime      = lookup' "loadtime"
                , turntime      = lookup' "turntime"
                , rows          = lookup' "rows"
                , cols          = lookup' "cols"
                , turns         = lookup' "turns"
                , playerSeed    = lookup' "player_seed"
                , viewradius2   = vr2
                , attackradius2 = ar2
                , spawnradius2  = sr2
                , viewCircle    = vp
                , attackCircle  = ap
                , spawnCircle   = sp
                }

distance :: GameParams -> Point -> Point -> Int
distance gp p1 p2 =
  let mp = (rows gp, cols gp)
  in manhattan mp p1 p2

--------------------------------------------------------------------------------
-- Game State ------------------------------------------------------------------
--------------------------------------------------------------------------------
data GameState = GameState
  { world     :: World
  , ants      :: [Ant]  -- call "ants GameState" to all ants
  , food      :: [Food] -- call "food GameState" to all food
  , hills     :: [Hill] -- call "hills GameState" to all hills
  , startTime :: UTCTime
  }

updateGameState :: [Point] -> GameState -> String -> GameState
updateGameState vp gs s
  | "f" `isPrefixOf` s = -- add food
      let p = toPoint.tail $ s
          fs' = p:food gs
          nw = writeTile (world gs) p FoodTile
      in GameState nw (ants gs) fs' (hills gs) (startTime gs)
  | "w" `isPrefixOf` s = -- add water
      let p = toPoint.tail $ s
          nw = writeTile (world gs) p Water
      in GameState nw (ants gs) (food gs) (hills gs) (startTime gs)
  | "h" `isPrefixOf` s = -- add hill
      let p = toPoint.init.tail $ s
          own = toOwner.digitToInt.last $ s
          hs = Hill { pointHill = p, ownerHill = own}:hills gs
          nw = writeTile (world gs) p $ HillTile own
      in GameState nw (ants gs) (food gs) hs (startTime gs)
  | "a" `isPrefixOf` s = -- add ant
      let own = toOwner.digitToInt.last $ s
          p = toPoint.init.tail $ s
          as' = Ant { pointAnt = p, ownerAnt = own}:ants gs
          nw = writeTile (world gs) p $ AntTile own
          nw' = if own == Me then addVisible nw vp p else nw
      in GameState nw' as' (food gs) (hills gs) (startTime gs)
  | "d" `isPrefixOf` s = -- add dead ant
      let own = toOwner.digitToInt.last $ s
          p = toPoint.init.tail $ s
          nw = writeTile (world gs) p $ Dead own
      in GameState nw (ants gs) (food gs) (hills gs) (startTime gs)
  | otherwise = gs -- ignore line
  where
    toPoint :: String -> Point
    toPoint = tuplify2.map read.words
    writeTile w p t = runSTArray $ do
      w' <- unsafeThaw w
      writeArray w' p MetaTile {tile = t, visibleMT = True}
      return w'

-- | Find the time remaining in the game
timeRemaining :: GameState -> IO NominalDiffTime
timeRemaining gs = do
  timeNow <- getCurrentTime
  return $ timeNow `diffUTCTime` startTime gs

--------------------------------------------------------------------------------
-- Orders ----------------------------------------------------------------------
--------------------------------------------------------------------------------
data Order = Order
  { ant :: Ant
  , directionOrder :: Direction
  } deriving (Show)

order2point :: Order -> Point
order2point o = move (directionOrder o) (pointAnt . ant $ o)

passable :: World -> Order -> Bool
passable w order =
  let newPoint = move (directionOrder order) (pointAnt $ ant order)
  in  tile (w %! newPoint) /= Water

occupied :: World -> Order -> Bool
occupied w order =
  let newPoint = move (directionOrder order) (pointAnt $ ant order)
      t = tile (w %! newPoint)
  in  isAnt t || t == FoodTile || t == Water

unoccupied :: World -> Order -> Bool
unoccupied w order = not $ occupied w order

issueOrder :: Order -> IO ()
issueOrder order = do
  let srow = (show . row . pointAnt . ant) order
      scol = (show . col . pointAnt . ant) order
      sdir = (show . directionOrder) order
  putStrLn $ "o " ++ srow ++ " " ++ scol ++ " " ++ sdir

--------------------------------------------------------------------------------
-- Updating Game ---------------------------------------------------------------
--------------------------------------------------------------------------------
type MWorld s = STArray s Point MetaTile

setVisible :: MWorld s -> Point -> ST s ()
setVisible mw p = do
  bnds <- getBounds mw
  let np = modPoint (incPoint $ snd bnds) p
  modifyWorld mw visibleMetaTile np

addVisible :: World
           -> [Point] -- ^ view points
           -> Point   -- ^ center point
           -> World
addVisible w vp p =
  runSTArray $ do
    w' <- unsafeThaw w
    mapM_ (setVisible w' . sumPoint p) vp
    return w'

initialWorld :: GameParams -> World
initialWorld gp = listArray ((0,0), (rows gp - 1, cols gp - 1)) $ repeat MetaTile {tile = Unknown, visibleMT = False}

modifyWorld :: MWorld s -> (MetaTile -> MetaTile) -> Point -> ST s ()
modifyWorld mw f p = do
  e' <- readArray mw p
  e' `seq` writeArray mw p (f e') -- !IMPORTANT! seq is necessary to avoid space leaks

mapWorld :: (MetaTile -> MetaTile) -> World -> World
mapWorld f w = runSTArray $ do
  mw <- unsafeThaw w
  mapM_ (modifyWorld mw f) (indices w)
  return mw

gameLoop :: GameParams
         -> (GameState -> IO [Order])
         -> World
         -> [String] -- input
         -> IO ()
gameLoop gp doTurn w (line:input)
  | "turn" `isPrefixOf` line = do
      hPutStrLn stderr line
      time <- getCurrentTime
      let cs = break (isPrefixOf "go") input
          gs = foldl' (updateGameState $ viewCircle gp) (GameState w [] [] [] time) (fst cs)
      orders <- doTurn gs
      mapM_ issueOrder orders
      finishTurn
      gameLoop gp doTurn (mapWorld clearMetaTile $ world gs) (tail $ snd cs) -- clear world for next turn
  | "end" `isPrefixOf` line = endGame input
  | otherwise = gameLoop gp doTurn w input
gameLoop _ _ _ [] = endGame []

game :: (GameParams -> GameState -> IO [Order]) -> IO ()
game doTurn = do
  content <- getContents
  let cs = break (isPrefixOf "ready") $ lines content
      gp = createParams $ map (tuplify2.words) (fst cs)
  finishTurn
  gameLoop gp (doTurn gp) (initialWorld gp) (tail $ snd cs)

-- TODO this could be better
endGame :: [String] -> IO ()
endGame input = do
  hPutStrLn stderr "end of game"
  mapM_ (hPutStrLn stderr) input

-- | Tell engine that we have finished the turn or setting up.
finishTurn :: IO ()
finishTurn = do
  putStrLn "go"
  hFlush stdout
