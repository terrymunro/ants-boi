{-# LANGUAGE Arrows #-}

module Game.World
  ( World
  , Owner
  , HasOwner (..)
  , Direction
  ) where

import Control.Arrow
import Data.Array (Array, assocs, bounds)

import Game.Point (Point, HasPoint (..), Column, Row, col, row)
import Game.Tile  (MetaTile (..), Owner (..), HasOwner (..))

--------------------------------------------------------------------------------
-- Direction -------------------------------------------------------------------
--------------------------------------------------------------------------------
data Direction = North | East | South | West deriving (Eq, Bounded, Enum)

instance Show Direction where
  show North = "N"
  show East  = "E"
  show South = "S"
  show West  = "W"

move :: Direction -> Point -> Point
move dir p
  | dir == North = fromTuple (row $ pred p, col p)
  | dir == South = fromTuple (row $ succ p, col p)
  | dir == West  = fromTuple (row p, col $ pred p)
  | otherwise    = fromTuple (row p, col $ succ p)

--------------------------------------------------------------------------------
-- Food ------------------------------------------------------------------------
--------------------------------------------------------------------------------
newtype Food = Food Point

instance HasPoint Food where
  pnt (Food p) = p

--------------------------------------------------------------------------------
-- Ants ------------------------------------------------------------------------
--------------------------------------------------------------------------------
data Ant = Ant
  { antPoint :: Point
  , antOwner :: Owner
  } deriving (Show, Eq)

instance HasPoint Ant where
  pnt = antPoint

instance HasOwner Ant where
  owner = antOwner

--------------------------------------------------------------------------------
-- Hills -----------------------------------------------------------------------
--------------------------------------------------------------------------------
data Hill = Hill
  { hillPoint :: Point
  , hillOwner :: Owner
  } deriving (Show, Eq)

instance HasPoint Hill where
  pnt = hillPoint

instance HasOwner Hill where
  owner = hillOwner

--------------------------------------------------------------------------------
-- Immutable World -------------------------------------------------------------
--------------------------------------------------------------------------------
newtype World = World (Array Point MetaTile)

-- | For debugging
instance Show World where
  show w =
    let maxCol = colBound w
        renderAssoc :: (Point, MetaTile) -> String
        renderAssoc a
          | col (fst a) == maxCol = renderTile (snd a) ++ "\n"
          | otherwise = renderTile (snd a)
    in concatMap renderAssoc (assocs w)

worldBounds :: World -> Point
worldBounds = (row &&& col) . snd . bounds

colBound :: World -> Column
colBound = col . snd . bounds

rowBound :: World -> Row
rowBound = row . snd . bounds

isVisibleIn :: World -> Point -> Bool
isVisibleIn world p = visibleMT $ world %! p

-- | Accesses World using the modulus of the point
(%!) :: World -> Point -> MetaTile
(%!) w p = w ! (w %!% p)

-- | Takes the modulus of the point
(%!%) :: World -> Point -> Point
(%!%) w p =
  let modCol = succ $ colBound w
      modRow = succ $ rowBound w
      ixCol  = col p `mod` modCol
      ixRow  = row p `mod` modRow
  in (ixRow, ixCol)

direction :: World -> Point -> Point -> (Maybe Direction, Maybe Direction)
direction world source dest
  | x1 == x2 = (Nothing, Just ydir)
  | y1 == y2 = (Just xdir, Nothing)
  | otherwise = (Just xdir, Just ydir)
  where rn = rowBound world
        cn = colBound world
        x1 = row source
        x2 = row dest
        xdir = if (abs $ x1 - x2) <= (rn `div` 2)
                then if x1 >= x2 then North else South
                else if x1 >= x2 then South else North
        y1 = col source
        y2 = col dest
        ydir = if (abs $ y1 - y2) <= (cn `div` 2)
                 then if y1 >= y2 then West else East
                 else if y1 >= y2 then East else West
