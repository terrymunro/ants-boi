module Game.Tile
  ( Tile (..)
  , Owner (..)
  , HasOwner (..)
  , MetaTile

  -- getters / setters
  , setVisible
  , clearVisible
  , toOwner

  -- predicates
  , isDead
  , isAnt
  , isAntEnemy
  , isHill
  , isHillEnemy
  , isDeadEnemy

  -- filters
  , mine
  , enemies
  ) where

import Data.Char  (toUpper)

import Game.Util  (fOr)

--------------------------------------------------------------------------------
-- Owner -----------------------------------------------------------------------
--------------------------------------------------------------------------------
data Owner = Me | Enemy Int deriving (Show, Eq)

class HasOwner a where
  owner    :: a -> Owner

  isMine'  :: a -> Bool
  isMine' = (==Me) . owner

  isEnemy' :: a -> Bool
  isEnemy' = not . isMine'

-- | Removes elements that are not owned by me
mine :: HasOwner a => [a] -> [a]
mine = filter isMine'

-- | Removes elements that are not enemies
enemies :: HasOwner a => [a] -> [a]
enemies = filter isEnemy'

toOwner :: Int -> Owner
toOwner 0 = Me
toOwner a = Enemy a

--------------------------------------------------------------------------------
-- Tiles -----------------------------------------------------------------------
--------------------------------------------------------------------------------
-- TODO: Lenses
data Tile = AntTile Owner
          | Dead Owner
          | HillTile Owner
          | Land
          | FoodTile
          | Water
          | Unknown
          deriving (Show,Eq)

-- | Elements of the world
data MetaTile = MetaTile
  { tile      :: Tile
  , visibleMT :: Bool
  }

instance Show MetaTile where
  -- | For debugging
  show m
    | tile m == AntTile Me  = visibleUpper m 'm'
    | isAntEnemy $ tile m   = visibleUpper m 'e'
    | tile m == Dead Me     = visibleUpper m 'd'
    | isDeadEnemy $ tile m  = visibleUpper m 'd'
    | tile m == Land        = visibleUpper m 'l'
    | tile m == HillTile Me = visibleUpper m 'h'
    | isHillEnemy $ tile m  = visibleUpper m 'x'
    | tile m == FoodTile    = visibleUpper m 'f'
    | tile m == Water       = visibleUpper m 'w'
    | otherwise             = "*"
    where
      visibleUpper :: MetaTile -> Char -> String
      visibleUpper mt c
        | visibleMT mt = [toUpper c]
        | otherwise    = [c]

isAnt :: Tile -> Bool
isAnt (AntTile _) = True
isAnt _ = False

isDead :: Tile -> Bool
isDead (Dead _) = True
isDead _ = False

isHill :: Tile -> Bool
isHill (HillTile _) = True
isHill _ = False

isAntEnemy :: Tile -> Bool
isAntEnemy (AntTile (Enemy _)) = True
isAntEnemy _ = False

isHillEnemy :: Tile -> Bool
isHillEnemy (HillTile (Enemy _)) = True
isHillEnemy _ = False

isDeadEnemy :: Tile -> Bool
isDeadEnemy (Dead (Enemy _)) = True
isDeadEnemy _ = False

-- TODO: Determine why were setting tile to land in this function
-- | Sets the tile to visible, if the tile is still unknown then it is land.
setVisible :: MetaTile -> MetaTile
setVisible m
  | tile m == Unknown = MetaTile {tile = Land,   visibleMT = True}
  | otherwise         = MetaTile {tile = tile m, visibleMT = True}

--------------------------------------------------------------------------------
-- | Resets tile to land if it is currently occupied by food or ant and makes
-- | the tile invisible.
clearVisible :: MetaTile -> MetaTile
clearVisible m
  | fOr (tile m) [isAnt, (==FoodTile), isDead]
              = MetaTile {tile = Land,   visibleMT = False}
  | otherwise = MetaTile {tile = tile m, visibleMT = False}
