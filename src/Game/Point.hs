{-# LANGUAGE FlexibleContexts #-}

module Game.Point
  ( Row
  , Column
  , Point
  , HasPoint (..)

  , row
  , col

  , manhattan
  , sumPoint
  , getPointCircle
  , modPoint
  , incPoint
  ) where

--------------------------------------------------------------------------------
-- Point -----------------------------------------------------------------------
--------------------------------------------------------------------------------
newtype Row    = Row Int deriving (Show, Eq)

instance Monoid Row where
  mempty = Row 0
  mappend (Row a) (Row b) = Row $ a + b

newtype Column = Column Int deriving (Show, Eq)

instance Monoid Column where
  mempty = Column 0
  mappend (Column a) (Column b) = Column $ a + b

newtype Point = Point (Row, Column) deriving (Show, Eq)

-- | Access to Point information through TC
class HasPoint a where
  pnt :: a -> Point

  row' :: a -> Int
  row' = row . pnt

  col' :: a -> Int
  col' = col . pnt

-- | Gets the row from a point
row :: Point -> Int
row (Point (Row r, _)) = r

-- | Gets the column from a point
col :: Point -> Int
col (Point (_, Column c)) = c

fromTuple :: (Int, Int) -> Point
fromTuple (r, c) = Point (Row r, Column c)

--------------------------------------------------------------------------------
-- Utils for Norms and Metrics -------------------------------------------------
-- https://secure.wikimedia.org/wikipedia/en/wiki/Norm_(mathematics) -----------
--------------------------------------------------------------------------------
modDistance :: Int            -- ^ Modulus
            -> Int
            -> Int
            -> Int
modDistance m x y =
  let a = abs $ x - y
  in min a (m - a)

-- | Computes manhattan distance
manhattan :: Point            -- ^ Modulus point
          -> Point
          -> Point
          -> Int
manhattan mp p1 p2 =
  let rowd = modDistance (row mp) (row p1) (row p2)
      cold = modDistance (col mp) (col p1) (col p2)
  in rowd + cold

-- | Computes the square of the two norm.
twoNormSquared :: Point -> Int
twoNormSquared p = row p ^ (2::Int) + col p ^ (2::Int)

sumPoint :: Monoid Point => Point -> Point -> Point
sumPoint x y = mappend x y

incPoint :: Enum Point => Point -> Point
incPoint p = succ p

-- | Calculate Modulus Point of two Points
modPoint :: Point
         -> Point
         -> Point            -- ^ Modulus point
modPoint mp p = Point (Row $ row p `mod` row mp, Column $ col p `mod` col mp)

-- | Get a circle of points?
getPointCircle :: Int        -- ^ The radius squared
               -> [Point]    -- ^ ???
getPointCircle r2 =
  let rx = truncate.sqrt.(fromIntegral::Int -> Double) $ r2
  in filter ((<=r2).twoNormSquared) $ fmap fromTuple $ (,) <$> [-rx..rx] <*> [-rx..rx]

